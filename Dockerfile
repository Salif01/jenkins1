FROM openjdk:8-jdk-alpine
VOLUME /temp
ADD target/jenkins*.jar /jenkins.jar
CMD ["java", "-jar", "/jenkins.jar", "—spring.profiles.active=prod"]
EXPOSE 8585

