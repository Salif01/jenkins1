// Declarative //
node {
    def app
        stage('clone') {
            checkout master
        }
        stage('Build image') {
            app=docker.build("jenkins")
        }
        stage('Run image') {
            docker.image('jenkins').withRun('-p 8383 : 8282'){ c->
                sh'docker ps'
            }
        }
}

